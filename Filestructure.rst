.. _Sphinx File Structure:

=====================
Sphinx File Structure
=====================

After successful completion of *sphinx-quickstart*, it creates
files and folders as shown below if all values are entered as 
Default.

.. image:: image/sphinxfilestructure.png

**config.py**

This a *Python* file contains all the configuration of the values 
which we feed during *sphinx-quickstart* command.

This *config.py* file now can be changed if necessary, like 
*Project Name*, *Authors*, adding *autodoc* or *mathjax sphinx 
extension* which would reflect in sphinx build.

.. image:: image/config.png

**index.rst**


*index.rst* is a *reStructuredText* (rst) file is a master document, 
the contents contained in the *index.rst* are displayed in the 
*Welcome* Page.

List of files can added using *toctree* directives, which is a *Sphinx*
custom directive for interconnecting Multiple files and to create *Table 
of content* in the *Welcome* page.

.. image:: image/index.png



