.. _bibliography:

============
Bibliography
============

#. http://sphinx-doc.org/intro.html
   
   Official Sphinx Documentation website
   
#. http://docutils.sourceforge.net/docs/user/rst/quickstart.html

   reStructuredText primer website, gives a Introdution to reStructuredText
   
   
#. http://www.ibm.com/developerworks/library/os-sphinx-documentation/#listing2
   
   "Easy and beautiful documentation with Sphinx" by Alfredo Deza, Software Engineer



