.. Sphinx Tutorial master file, created by
   sphinx-quickstart on Sat May 24 10:54:53 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

====================
Sphinx Documentation
====================

Sphinx is a programming tool developed for generating *Python* software 
documentation intended for documenting Application Prpgramming Interface(API) 
or end users Guide, or both, from a set of specially commented *Python* 
source code files.

Sphinx uses *reStructuredText* in short *reST* as *Markup 
Language* for document generation and produce various file 
formats such as HTML, PDF, EPub, Texinfo, manual pages, plain text

Sphinx supports various extensions, *autodoc* for autogenerating documentation 
from *Python* source code, *mathjax* for mathematical notation.

**Example of Sphinx File**
::

   
    This is a Title
    ===============
    That has a paragraph about a main subject and is set when the '='
    is at least the same length of the title itself.

    Subject Subtitle
    ----------------
    Subtitles are set with '-' and are required to have the same length 
    of the subtitle itself, just like titles.

    Lists can be unnumbered like:

     * Item Foo
     * Item Bar

    Or automatically numbered:

     #. Item 1
     #. Item 2

    Inline Markup
    -------------
    Words can have *emphasis in italics* or be **bold** and you can define
    code samples with back quotes, like when you talk about a command: ``sudo`` 
    gives you super user powers!
    
    
**Output of the above sphinx file**

.. image:: image/output_rest.png
    


.. toctree::
   :maxdepth: 2

   Quickstart
   sphinx-quickstart
   Filestructure
   bibliography

