.. _Sphinx Quickstart:

=================
Sphinx Quickstart
=================

Now, we look into more details of *sphinx-quickstart* command.

**Root path for the documentation**

::

    $ sphinx-quickstart 
    Welcome to the Sphinx 1.2.2 quickstart utility.

    Please enter values for the following settings (just press Enter to
    accept a default value, if one is given in brackets).

    Enter the root path for documentation.
    > Root path for the documentation [.]: 

When you enter *sphinx-quickstart* it asks for the *Root path for 
the documentation*, a Directory where would you like to
add the Sphinx Documenations Files.

The default values are in Square Bracket [.] as in the *Root path for 
the documentation*, the current Directory. 

The Dot '.' which is set as default value and  this should be ok to use the 
current directory or enter a new directory like "/home/.../tutorial".   

**Separate source and build directories**

::
 
    > Separate source and build directories (y/n) [n]: 
    
You have two options for placing the build directory for Sphinx output.
Either, you use a directory "_build" within the root path, or you separate
"source" and "build" directories within the root path. 

**Name prefix for templates and static dir**

::

    > Name prefix for templates and static dir [_]: 

Inside the root directory, two more directories will be created; "_templates"
for custom HTML templates and "_static" for custom stylesheets and other static
files. You can enter another prefix (such as ".") to replace the underscore.


**Project name & Author name(s)**

::

    > Project name: Sphinx Tutorial
    > Author name(s): xxxxxx, yyyyyyyy

Add Project name & Author name(s).The project name will occur in several 
places in the built documentation.


**Project version & release**

::

    > Project version: 0.1
    > Project release [0.1]: 

Sphinx has the notion of a "version" and a "release" for the
software. Each version can have multiple releases. For example, for
Python the version is something like 2.5 or 3.0, while the release is
something like 2.5.1 or 3.0a1.  If you don't need this dual structure,
just set both to the same value.


**Source file suffix**

::

    > Source file suffix [.rst]: 

The file name suffix for source files. Commonly, this is either ".txt"
or ".rst".  Only files with this suffix are considered documents.

**Name of your master document**

::

    > Name of your master document (without suffix) [index]: 
    
One document is special in that it is considered the top node of the
"contents tree", that is, it is the root of the hierarchical structure
of the documents. Normally, this is "index", but if your "index"
document is a custom template, you can also set this to another filename.

**epub builder**  

::

    > Do you want to use the epub builder (y/n) [n]: 

epub is *Electronic Publishing*  is a free and open e-book standard by the
International Digital Publishing Forum. 

Files have the extension *.epub*.

Sphinx can also add configuration for epub output


**Sphinx extensions**

::

    Please indicate if you want to use one of the following Sphinx extensions:
    > autodoc: automatically insert docstrings from modules (y/n) [n]: 
    > doctest: automatically test code snippets in doctest blocks (y/n) [n]: 
    > intersphinx: link between Sphinx documentation of different projects (y/n) [n]: 
    > todo: write "todo" entries that can be shown or hidden on build (y/n) [n]: 
    > coverage: checks for documentation coverage (y/n) [n]: 
    > pngmath: include math, rendered as PNG images (y/n) [n]: 
    > mathjax: include math, rendered in the browser by MathJax (y/n) [n]: 
    > ifconfig: conditional inclusion of content based on config values (y/n) [n]: 
    > viewcode: include links to the source code of documented Python objects (y/n) [n]:
    
Sphinx extension allows special features which can modify any aspect of documentation during
build. For example *autodoc*, which can automatically pull docstrings of a *Python class* using a 
directive -  *.. _autoclass::*

Refer `Sphinx extension <http://sphinx-doc.org/extensions.html>`_ 
gives more detailed explaination on each extension & how to develop a new extension for sphinx
Documentation.


**Makefile**

:: 

    > Create Makefile? (y/n) [y]: 
    > Create Windows command file? (y/n) [y]:
 
A Makefile and a Windows command file can be generated for you so that you only have to 
run e.g. 'make html' instead of invoking sphinx-build directly.

At the end of *sphinx-quickstart* it creates

	* conf.py - which is a configuration file.
	
	* index.rst - where it reads list of  file for *sphinx* build.
	
	* Makefile - automatic build execution.

::

    Creating file ./conf.py.
    Creating file ./index.rst.
    Creating file ./Makefile.
    Creating file ./make.bat.

    Finished: An initial directory structure has been created.

    You should now populate your master file ./index.rst and create other documentation
    source files. Use the Makefile to build the docs, like so:
       make builder
    where "builder" is one of the supported builders, e.g. html, latex or linkcheck.
    



